//
//  Assignment001UITests.swift
//  Assignment001UITests
//
//  Created by Chenxing Mei on 22/09/16.
//  Copyright © 2016 Chenxing Mei. All rights reserved.
//

import XCTest

class Assignment001UITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        
        //make sure the drawing app is login, else error
        
        let app = XCUIApplication()
        app.buttons["draw64"].tap()
        app.buttons["righttriangle64"].tap()
        app.buttons["rectangle64"].tap()
        app.buttons["circle64"].tap()
        app.buttons["line64"].tap()
        
        let element = app.otherElements.containingType(.NavigationBar, identifier:"Assignment001.View").childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.childrenMatchingType(.Other).elementBoundByIndex(2)
        element.childrenMatchingType(.Button).elementBoundByIndex(17).tap()
        element.childrenMatchingType(.Button).elementBoundByIndex(16).tap()
        element.childrenMatchingType(.Button).elementBoundByIndex(15).tap()
        element.childrenMatchingType(.Button).elementBoundByIndex(14).tap()
        element.childrenMatchingType(.Button).elementBoundByIndex(13).tap()
        element.childrenMatchingType(.Button).elementBoundByIndex(12).tap()
        app.buttons["change"].tap()
        app.buttons["eraser"].tap()
        
        let save64Button = app.buttons["save64"]
        save64Button.tap()
        
        let saveButton = app.alerts["Save drawing"].collectionViews.buttons["Save"]
        saveButton.tap()
        save64Button.tap()
        saveButton.tap()
        app.buttons["logout"].tap()
        app.alerts["ALert"].collectionViews.buttons["Cancel"].tap()
        
        
    }
    
}
