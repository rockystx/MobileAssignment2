//
//  ViewController.swift
//  Assignment001
//
//  Created by Chenxing Mei on 31/08/16.
//  Copyright © 2016 Chenxing Mei. All rights reserved.
//

import UIKit
import Social

class ViewController: UIViewController {
    
    var startPoint : CGPoint = CGPointZero
    var layer : CAShapeLayer?
    var fillColorStatus = false
    var isEraserPressed = false
    var fillColor = UIColor.blackColor().CGColor
    var lineColor = UIColor.blackColor().CGColor
    var shape = Shape.Draw
    var pointArray = Array<CGPoint>()
   
    var lineWidth: CGFloat = 2.0
    
    @IBOutlet weak var drawView: UIView!


    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        removeall()
        
        if(!NSUserDefaults.standardUserDefaults().boolForKey("isUserLoggedIn"))
        {
             self.performSegueWithIdentifier("loginView", sender: self)
        }
        
    }
    
    
    
    // list of color and shape
    enum Color : Int
    {
        case Black = 0
        case Blue
        case Orange
        case Brown
        case Pink
        case Green
    }
    
    enum Shape : Int
    {
        case Draw = 0
        case Triangle
        case Retangle
        case Circle
        case Line
        case Eraser
    }
    
    enum Functions : Int
    {
        case Switch = 0
        case Trash
        case Save
        case Facebook
        case Twitter
        case Logout
    }
    
    
    //set lineColor and fillColor for each color selection
    @IBAction func selectColor(sender: UIButton) {
        sender.showsTouchWhenHighlighted = true
        
        switch sender.tag
        {
        case Color.Pink.rawValue:
            fillColor = UIColor(red: 255/255.0, green: 192/255.0, blue: 203/255.0, alpha: 1).CGColor
            
        case Color.Orange.rawValue:
            fillColor = UIColor.orangeColor().CGColor
            
        case Color.Brown.rawValue:
            fillColor = UIColor.brownColor().CGColor
            
        case Color.Blue.rawValue:
            fillColor = UIColor.blueColor().CGColor
            
        case Color.Black.rawValue:
            fillColor = UIColor.blackColor().CGColor
            
        case Color.Green.rawValue:
            fillColor = UIColor.greenColor().CGColor
            
        default: break
        }
        lineColor = fillColor
        
    }
    
    
    //set line shape
    @IBAction func selectShape(sender: UIButton) {
        sender.showsTouchWhenHighlighted = true
        
        

        switch sender.tag
        {
        case Shape.Line.rawValue:
            shape = Shape.Line
        case Shape.Circle.rawValue:
            shape = Shape.Circle
        case Shape.Retangle.rawValue:
            shape = Shape.Retangle
        case Shape.Triangle.rawValue:
            shape = Shape.Triangle
        case Shape.Draw.rawValue:
            shape = Shape.Draw
            fillColorStatus = false
        case Shape.Eraser.rawValue:
            shape = Shape.Eraser
            fillColorStatus = false
            isEraserPressed = true
            
            
        default: break
        }
    }
    
    
    @IBAction func functionButton (sender: UIButton)
    {
        sender.showsTouchWhenHighlighted = true
        switch sender.tag
        {
        case Functions.Switch.rawValue:
            if(fillColorStatus)
            {
                fillColorStatus = false
            } else {
                if shape == Shape.Draw
                {
                    fillColorStatus = false
                }
                else
                {
                    fillColorStatus = true
                }
            }
        
        case Functions.Trash.rawValue:
        
            
            // display alert message
            let alert = UIAlertController(title: "Remove drawing", message: "Are you sure to remove the drawing?", preferredStyle: UIAlertControllerStyle.ActionSheet)
            let okAction = UIAlertAction(title: "Confirm", style: UIAlertActionStyle.Default, handler: {(alert: UIAlertAction!) in self.removeall()})
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true, completion: nil)
        
        case Functions.Save.rawValue:
            
            let alert = UIAlertController(title: "Save drawing", message: "Do you wish to save your drawing?", preferredStyle: UIAlertControllerStyle.ActionSheet)
            let saveAction = UIAlertAction(title: "Save", style: UIAlertActionStyle.Default){action in self.saveImage()}
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
            alert.addAction(saveAction)
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true, completion: nil)
            

        case Functions.Facebook.rawValue:
            
            //render layer into image first
            
            
            
            //declare a facebook share
            var shareToFacebook : SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
           
            //initial share content
            
            shareToFacebook.setInitialText("This is my drawing")
            shareToFacebook.addImage(renderImage())
            
            
             self.presentViewController(shareToFacebook, animated: true, completion: nil)
            
            
        case Functions.Twitter.rawValue:
            
     
            
            //declare a facebook share
            var shareToTwitter: SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            
            //initial share content
            
            shareToTwitter.setInitialText("This is my drawing")
            shareToTwitter.addImage(renderImage())
            
            
            self.presentViewController(shareToTwitter, animated: true, completion: nil)
            
            

            
        case Functions.Logout.rawValue:
            //display alert
            
            func logout()
            {
                NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isUserLoggedIn")
              //  NSUserDefaults.standardUserDefaults().synchronize()
                self.performSegueWithIdentifier("loginView", sender: self)
            }
            let myALert = UIAlertController(title: "WARNING! IMPORTANT", message: "Do you wish to log out now?", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "Confirm", style: .Destructive){action in logout()}
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
            myALert.addAction(okAction)
            myALert.addAction(cancelAction)
            self.presentViewController(myALert, animated: true, completion: nil)
        default: break
        }
    }
    
   
    
    @IBAction func handlePan(sender: UIPanGestureRecognizer)
    {
        if sender.state == .Began
        {
            startPoint = sender.locationInView(self.drawView)
            layer = CAShapeLayer()
            layer?.lineWidth = lineWidth
            if fillColorStatus == false {layer?.fillColor = nil}
            else if fillColorStatus != false{ layer?.fillColor = fillColor}
            layer?.opacity = 0.7
            layer?.strokeColor = lineColor
            self.drawView.layer.addSublayer(layer!)
            pointArray.removeAll()
            pointArray.append(startPoint)

            
        }
            
        else if sender.state == .Changed
        {
            let translation = sender .translationInView(self.drawView)
            
            let newPoint = sender.locationInView(self.drawView)
            
            switch shape
            {
            case Shape.Line:
                let temp = UIBezierPath()
                // pin out the two points forming a line
                temp.moveToPoint(CGPoint(x:startPoint.x, y:startPoint.y))
                temp.addLineToPoint(CGPoint(x:(newPoint.x), y:(newPoint.y)))
                temp.closePath()
                layer?.path = temp.CGPath
                
                
            case Shape.Triangle:
                let temp = UIBezierPath()
                // pin out the three points forming a triangle
                temp.moveToPoint(CGPoint(x:startPoint.x, y:startPoint.y))
                temp.addLineToPoint(CGPoint(x:(startPoint.x + translation.x), y:startPoint.y))
                temp.addLineToPoint(CGPoint(x:(startPoint.x + newPoint.x)/2, y: newPoint.y))
                temp.closePath()
                layer?.path = temp.CGPath
                
            case Shape.Retangle:
                layer?.path = (UIBezierPath(rect: CGRectMake(startPoint.x, startPoint.y, (newPoint.x - startPoint.x), (newPoint.y - startPoint.y)))).CGPath
                
                
            case Shape.Circle:
                layer?.path = (UIBezierPath(ovalInRect: CGRectMake(startPoint.x, startPoint.y, (newPoint.x - startPoint.x), (newPoint.y - startPoint.y)))).CGPath
                
            case Shape.Draw:
                
                pointArray.append(newPoint)
                let temp = UIBezierPath()
                temp.moveToPoint(CGPoint(x:pointArray[0].x, y:pointArray[0].y))
                for point in pointArray{
                    temp.addLineToPoint(CGPoint(x:(point.x), y:(point.y)))
                    temp.stroke()
                }
                
                layer?.path = temp.CGPath
            case Shape.Eraser:
                layer?.fillColor = nil
                layer?.strokeColor = UIColor.whiteColor().CGColor
                layer?.lineWidth = 25.0
                layer?.opacity = 1
                pointArray.append(newPoint)
                let temp = UIBezierPath()
                
                temp.moveToPoint(CGPoint(x:pointArray[0].x, y:pointArray[0].y))
                for point in pointArray{
                    temp.addLineToPoint(CGPoint(x:(point.x), y:(point.y)))
                    temp.stroke()
                }
                layer?.path = temp.CGPath
            }//end switch
            
        }
        
        
    }//end panhandle
    
    //share in facebook and twtter
    
    
    ///log out
    
    private func removeall()
    {
        self.drawView.layer.sublayers?.forEach{$0.removeFromSuperlayer()}
    }
    
    private func saveImage() {UIImageWriteToSavedPhotosAlbum(renderImage(), self, Selector(), nil)}

    private func renderImage() -> UIImage {
        UIGraphicsBeginImageContext(drawView.bounds.size)
        drawView.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let resultImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        return resultImage}

    
}//end class



