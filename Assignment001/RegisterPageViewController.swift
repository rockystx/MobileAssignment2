//
//  RegisterPageViewController.swift
//  Assignment001
//
//  Created by Chenxing Mei on 22/09/16.
//  Copyright © 2016 Chenxing Mei. All rights reserved.
//

import UIKit

class RegisterPageViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var occupationList = ["Driver", "Lecture", "Student", "Self-Employee", "General Labor", "Archetecture", "Lawyer", "Doctor", "Farmer", "Manager", "Waiter", "Police"]
    var ageList = ["0-9", "10-18", "19-29", "30-45", "46-55", "56 and above", "N/A", "N/A", "N/A", "N/A", "N/A", "N/A"]
    var genderList = ["Male", "Female"]
    var occupation: String = ""
    var age: String = ""
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var occupationPick: UIPickerView!
    @IBOutlet weak var occupationTextField: UITextField!
    @IBOutlet weak var ageTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        occupationPick.delegate = self
        occupationPick.dataSource = self
        //ageTextField.delegate = self
        //occupationTextField.delegate = self
        occupationTextField.inputView = occupationPick
        
        

        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    
    // returns the number of 'columns' to display.
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
     return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
      if ageTextField.isFirstResponder()
      {return ageList.count}
        else
      {return occupationList.count}
    }
    
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
              if ageTextField.isFirstResponder()
              {
                age = ageList[row]
        }
              else {
    occupation = occupationList[row]
                occupationTextField.text = occupation
                ageTextField.text = age
        }
        


        
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    
        if ageTextField.isFirstResponder()
        {
            return ageList[row]
        }
        else {
        return occupationList[row]
        }

    }
    
 
    
    @IBAction func genderSwitch(sender: UISwitch) {
        if sender.on
        {
            genderLabel.text = "Male"
        }
        else{
            genderLabel.text = "Female"
        }
    }
    
    @IBAction func registerButtonPressed(sender: UIButton) {
        
        let username = userNameTextField.text
        let password = passwordTextField.text
        let gender = genderLabel.text
        age = ageTextField.text!
        let defaultUsername = NSUserDefaults.standardUserDefaults()
        
        //check for minimum length
        if (username?.characters.count < 6 || password?.characters.count < 6)
        {
            //display alert
            displayMyAlertMessage("Alert", userMessage: "Username and password shall be no less than 6 characters")
            return
        }
        
        //check for duplicate username
        
        
        else if (username == defaultUsername.stringForKey("usernameKey"))
        {
            displayMyAlertMessage("Username Verification Failed", userMessage: "Username already exist, please change another")

        }
        /*
        //check password validation
        else if (checkForSufficientComplexity(password!) == false)
        {
            displayMyAlertMessage("Password Verification Failed", userMessage: "Password shall have at least one uppercase, one lower case, a digital and a special character")
        }
        */
        
        //save username & password
        NSUserDefaults.standardUserDefaults().setObject(username, forKey: "usernameKey")
        NSUserDefaults.standardUserDefaults().setObject(password, forKey: "passwordKey")
        NSUserDefaults.standardUserDefaults().setObject(gender, forKey: "genderKey")
        NSUserDefaults.standardUserDefaults().setObject(age, forKey: "ageKey")
        NSUserDefaults.standardUserDefaults().setObject(occupation, forKey: "occupationKey")
        NSUserDefaults.standardUserDefaults().synchronize()
        
        
        //display confirmation message and close registration page
        
        let myALert = UIAlertController(title: "ALert", message: "Congratuations, your registration is successful", preferredStyle: UIAlertControllerStyle.Alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default){action in self.dismissViewControllerAnimated(true, completion: nil)}
        myALert.addAction(okAction)
        self.presentViewController(myALert, animated: true, completion: nil)
    
    }
    
    
    
    //functions to pop up alert message
    func displayMyAlertMessage(userTitle: String, userMessage: String)
    {
        let myALert = UIAlertController(title: userTitle, message: userMessage, preferredStyle: UIAlertControllerStyle.Alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
        myALert.addAction(okAction)
        self.presentViewController(myALert, animated: true, completion: nil)
    }
    
    
    //function used to check if text contains certain character
    func checkForSufficientComplexity(text : String) -> Bool{
        
        //check if text contains uppercase
        let capitalLetterRegEx  = ".*[A-Z]+.*"
        let texttest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
        let capitalresult = texttest.evaluateWithObject(text)

        
        //check if text contains number
        let numberRegEx  = ".*[0-9]+.*"
        let texttest1 = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        let numberresult = texttest1.evaluateWithObject(text)

        
        //check if text contains special character
        let specialCharacterRegEx  = ".*[!&^%$#@()/]+.*"
        let texttest2 = NSPredicate(format:"SELF MATCHES %@", specialCharacterRegEx)
        let specialresult = texttest2.evaluateWithObject(text)

        
        return capitalresult && numberresult && specialresult
        
    }
    @IBAction func goBacktoLogin(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
