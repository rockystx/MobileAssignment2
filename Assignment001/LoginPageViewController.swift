//
//  LoginPageViewController.swift
//  Assignment001
//
//  Created by Chenxing Mei on 22/09/16.
//  Copyright © 2016 Chenxing Mei. All rights reserved.
//

import UIKit

class LoginPageViewController: UIViewController {

    @IBOutlet weak var inputUsernameTextField: UITextField!
    
    @IBOutlet weak var inputPasswordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
   
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func loginPressed(sender: UIButton) {
        
        let username = inputUsernameTextField.text
        let password = inputPasswordTextField.text
        
        let usernameStored = NSUserDefaults.standardUserDefaults().stringForKey("usernameKey")
        let passwordStored = NSUserDefaults.standardUserDefaults().stringForKey("passwordKey")
        
        if usernameStored == username
        {
            if passwordStored == password
            {
                //display login successful page
                NSUserDefaults.standardUserDefaults().setBool(true, forKey: "isUserLoggedIn")

            
                self.dismissViewControllerAnimated(true, completion: nil)
                
            }
            else{
                displayAlertMessage("Incorrect password, plesase try again")

            }
        }
        else{
            displayAlertMessage("Username not exist, plesase try again")
            
        }

        
        
    }
    func displayAlertMessage(message: String)
    {
        let myALert = UIAlertController(title: "ALert", message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
        myALert.addAction(okAction)
        self.presentViewController(myALert, animated: true, completion: nil)
        
    }
    
    
    
}
